/**
 * Copyright(C) 2020 Luvina Software
 * BookUnSupportedPatchExeption.java , Nov 16, 2020 DucDT
 */
package com.techja.book.demo.error;

import java.util.Set;

/**
 * @author dinhtrongduc
 *
 */
public class BookUnSupportedFieldPatchException extends RuntimeException {
	
	public BookUnSupportedFieldPatchException(Set<String> keys) {
        super("Field " + keys.toString() + " update is not allow.");
    }

}
