/**
 * Copyright(C) 2020 Luvina Software
 * EditController  25 DucDT
 */
package com.techja.book.demo.controller;

import com.techja.book.demo.entities.Book;
import com.techja.book.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class EditController {
    @Autowired
    BookService bookService;
    @RequestMapping(value = "/edit", method= RequestMethod.POST)
    public  String editBook(Model model, @ModelAttribute("book")Book book){
        bookService.saveOrUpdate(book,  book.getId());
        String massage = "Edit thành công :  " + book.getName();
        model.addAttribute("massage",massage);

        return "ViewBook";
    }

}
