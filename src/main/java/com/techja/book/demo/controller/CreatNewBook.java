package com.techja.book.demo.controller;

import com.techja.book.demo.entities.Book;
import com.techja.book.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CreatNewBook {
    @Autowired
    BookService bookService;
@RequestMapping(value = "/addNewBook.do")
public  String addNewBook(Model model ){
    Book book = new Book();
    model.addAttribute("book", book);
    return "CreatNewBook";
}
    @RequestMapping(value = "/saveBook.do", method = RequestMethod.POST)
    public  String save(Model model , @ModelAttribute ("book")Book book){
        System.out.println("id " +book.getId());
        System.out.println("name " +book.getName());
        bookService.saveOrUpdate(book,  book.getId());
        String massage = "Save thành công :  " + book.getName();
        model.addAttribute("massage",massage);
        return "CreatNewBook";
    }

}
