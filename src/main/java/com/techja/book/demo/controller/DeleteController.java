/**
 * Copyright(C) 2020 Luvina Software
 * DeleteController  25 DucDT
 */
package com.techja.book.demo.controller;

import com.techja.book.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DeleteController {
    @Autowired
    BookService bookService;

    @RequestMapping(value = "/delete")
    public String delete(Model model, @RequestParam("id") String idModel) {
        System.out.println(" id model " + idModel);
        int id = Integer.parseInt(idModel);
        bookService.deleteBook(id);
        System.out.println("okekkekekeeke");
        return "redirect:/viewListBooks";
    }
}
