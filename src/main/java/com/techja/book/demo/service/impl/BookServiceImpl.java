package com.techja.book.demo.service.impl;

import com.techja.book.demo.entities.Book;
import com.techja.book.demo.error.BookNotFoundException;
import com.techja.book.demo.repository.BookRepository;
import com.techja.book.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
@Transactional
public class BookServiceImpl implements BookService {
    @Autowired
    private BookRepository repository;

    @Override
    public List<Book> findAll() {
        return repository.findAll();
    }
    @Override
    public Book newBook(Book newBook) {

        return repository.save(newBook);
    }
    @Override
    public Book findOne(int id) {
        return repository.findById(id).orElseThrow(() -> new BookNotFoundException(id));
    }
    @Override
    public Book saveOrUpdate(Book newBook, int id) {

        return repository.findById(id)
                .map(x -> {
                    x.setName(newBook.getName());
                    x.setAuthor(newBook.getAuthor());
                    x.setPrice(newBook.getPrice());
                    return repository.save(x);
                })
                .orElseGet(() -> {
                    newBook.setId(id);
                    return repository.save(newBook);
                });
    }
    @Override
    public void deleteBook(@PathVariable int id) {
        repository.deleteById(id);
    }
}
