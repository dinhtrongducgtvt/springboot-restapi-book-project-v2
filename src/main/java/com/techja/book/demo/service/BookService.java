package com.techja.book.demo.service;

import com.techja.book.demo.entities.Book;
import com.techja.book.demo.error.BookNotFoundException;
import com.techja.book.demo.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
@Transactional
public interface BookService {

    public List<Book> findAll();
    public Book newBook( Book newBook);
    public  Book findOne( int id);
    public Book saveOrUpdate( Book newBook,  int id);
    public void deleteBook(@PathVariable int id);
}
